# RocketMQ安装配置

## 安装版本

- RocketMQ: rocketmq-all-4.3.0-bin-release
- rocketmq-console: rocketmq-console-ng-1.0.0

## 安装路径

- RockerMQ: /opt/rockermq
- rocketmq-console: /opt/rocketmq-console

## RocketMQ的安装配置步骤

- 先确定已配置Java运行环境；
- 下载[安装包](http://mirror.bit.edu.cn/apache/rocketmq/4.3.0/rocketmq-all-4.3.0-bin-release.zip)；
- 解压到服务器目录`/opt/rocketmq`：

```bash
cd ~
unzip ~/rocketmq-all-4.3.0-bin-release.zip
mv ~/rocketmq-all-4.3.0-bin-release /opt/rocketmq
```

- 为可执行文件添加执行权限：

```bash
chmod +x /opt/rocketmq/bin/*
```

- 配置环境变量，在`/etc/profile`末尾追加：

```bash
export ROCKETMQ_HOME=/opt/rocketmq
export PATH=$ROCKETMQ_HOME/bin:$PATH
```

- 使环境变量生效：

```bash
source /etc/profile
```

- 创建日志目录：

```bash
mkdir /opt/rocketmq/logs
```

- 配置mqbroker，在`/opt/rocketmq/conf/2m-noslave/broker-a.properties`追加ip和端口（listenPort为mqbroker端口，namesrvAddr为Name Server地址，其中ip为NameServer本机IP）：

```properties
listenPort=10911
namesrvAddr=127.0.0.1:9876
```

> 注意：RocketMQ的脚本对JVM的默认参数都设置得很大，可能会导致无法分配内存而启动失败，故启动前要按需调整JVM参数（参数配置分别在`/opt/rocketmq/bin/runserver.sh`和`/opt/rocketmq/bin/runbroker.sh`里），启动后注意看一下log是否启动成功。

- 启动Name Server:

```bash
nohup sh /opt/rocketmq/bin/mqnamesrv > /opt/rocketmq/logs/mqnamesrv.log 2>/opt/rocketmq/logs/mqnamesrv-err.log &
```

- 查看日志是否启动成功，`tail /opt/rocketmq/logs/mqnamesrv.log`：

```log
The Name Server boot success. serializeType=JSON
```

- 启动Broker：

```bash
nohup sh /opt/rocketmq/bin/mqbroker -c /opt/rocketmq/conf/2m-noslave/broker-a.properties > /opt/rocketmq/logs/mqbroker.log 2> /opt/rocketmq/logs/mqbroker-err.log &
```

- 查看日志是否启动成功，`tail /opt/rocketmq/logs/mqbroker.log`：

```log
The broker[broker-a, 172.18.151.149:10911] boot success. serializeType=JSON and name server is 127.0.0.1:9876
```

- 到此启动RocketMQ成功。

---

## rocketmq-console安装配置步骤

- 先在本地下载[源码](https://github.com/apache/rocketmq-externals/archive/rocketmq-console-1.0.0.zip)用于编译打包；
- 解压后修改源码配置文件`rocketmq-console/src/main/resources/application.properties`中的下列对应项的值（可根据需要进行修改）：

```properties
server.contextPath=/rocketmq
server.port=9888
rocketmq.config.namesrvAddr=127.0.0.1:9876
```

- 编译:

```bash
cd rocketmq-console
mvn clean package -DskipTests
```

- 在服务器上建立目录`/opt/rocketmq-console`，并把编译结果`rocketmq-console/target/rocketmq-console-ng-1.0.0.jar`放到服务器上的该目录中；
- 启动rocketmq-console服务：

```bash
nohup java -jar /opt/rocketmq-console/rocketmq-console-ng-1.0.0.jar >/opt/rocketmq-console/rocketmq-console.log 2>&1 &
```

- 启动成功后通过`${ip}:9888/rocketmq`即可访问rocket-console，不过由于是内网，外网需要通过NginX做一层转发才可以访问。

---

## 自启动配置

- 配置RocketMQ启动脚本，在`/opt/rocketmq/bin/`下添加脚本文件`mqstart.sh`并授予执行权限：

```bash
#!/bin/bash

nohup sh /opt/rocketmq/bin/mqnamesrv >/opt/rocketmq/logs/mqnamesrv.log 2>/opt/rocketmq/logs/mqnamesrv-err.log &
nohup sh /opt/rocketmq/bin/mqbroker -c /opt/rocketmq/conf/2m-noslave/broker-a.properties >/opt/rocketmq/logs/mqbroker.log  2>/opt/rocketmq/logs/mqbroker-err.log &
```

```bash
chmod +x /opt/rocketmq/bin/mqstart.sh
```

- 添加自启动，在`/etc/rc.local`文件里追加（注意确保该文件有可执行权限）：

```bash
# RocketMQ rocketmq-console自启动
su - tsintergy -c /opt/rocketmq/bin/mqstart.sh &
su - tsintergy -c "nohup java -jar /opt/rocketmq-console/rocketmq-console-ng-1.0.0.jar >/opt/rocketmq-console/rocketmq-console.log 2>&1 &" &
```
