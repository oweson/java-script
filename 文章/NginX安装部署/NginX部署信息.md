# NginX安装部署

- 下载[源码包](https://github.com/nginx/nginx/archive/release-1.14.2.tar.gz)和第三方模块[一致性哈希](https://github.com/replay/ngx_http_consistent_hash/archive/master.zip)；
- 在服务器上解压：

```bash
tar zxvf release-1.14.2.tar.gz
unzip master.zip
```

- 编译安装：

```bash
cd nginx-release-1.14.2
# 如没有安装C的编译器，请先安装：
yum -y install gcc-c++
# 安装依赖库，如后面命令仍提示缺少其他库，请自行安装：
yum -y install pcre-devel openssl openssl-devel
# 配置，指定需要的模块/第三方模块：
./auto/configure --add-module=../ngx_http_consistent_hash-master --with-stream --with-stream_ssl_module --with-mail --with-mail_ssl_module --with-http_xslt_module --with-http_ssl_module --with-http_image_filter_module
# 编译：
make
# 安装：
make install
```

- 顺利执行完以上命令后，NginX会默认安装在`/usr/local/nginx`目录中，执行目录中的`sbin/nginx`即可启动NginX；
- 配置自启动，在`/etc/rc.local`追加以下命令（注意该文件是否有可执行权限，如没有则需要额外添加）：

```bash
# NginX自启动
su -c /usr/local/nginx/sbin/nginx &
```

### 相关命令

```bash
# 启动：
sudo /usr/local/nginx/sbin/nginx
# 关闭：
sudo /usr/local/nginx/sbin/nginx -s stop
# 重启：
sudo /usr/local/nginx/sbin/nginx -s reload
```