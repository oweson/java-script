import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author oweson
 * @date 2022/6/5 16:46
 */


public class ThreaadPoolTest implements Runnable, Callable {
    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        executorService.execute(new Thread(new ThreaadPoolTest()));
        executorService.execute(new Thread(new ThreaadPoolTest()));


    }




    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName()+": "+i);
        }

    }

    @Override
    public String call() throws Exception {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName()+": "+i*i);
        }
        return "";
    }
}
