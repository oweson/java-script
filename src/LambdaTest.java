import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author oweson
 * @date 2022/6/13 21:58
 */


public class LambdaTest {
    public static void main(String[] args) {
       /* Map<Integer,UserBean> map = new HashMap<>();
        UserBean userBean = new UserBean();
        userBean.setId(0);
        userBean.setName("one");
        UserBean userBeanSecond = new UserBean();
        userBean.setId(0);
        userBean.setName("second");
        map.put(1,userBean);
        map.put(2,userBeanSecond);

        for (Map.Entry<Integer, UserBean> item : map.entrySet()) {
            UserBean value = item.getValue();


        }*/
        List<UserBean> userBeanList = new ArrayList<>();
        UserBean userBean = new UserBean();
        userBean.setId(1);
        userBean.setName("one");
        UserBean userBeanSecond = new UserBean();
        userBeanSecond.setId(2);
        userBeanSecond.setName("second");
        UserBean userBeanThree = new UserBean();
        userBeanThree.setId(2);
        userBeanThree.setName("three");
        userBeanList.add(userBean);
        userBeanList.add(userBeanSecond);
        userBeanList.add(userBeanThree);
        Map<Integer, List<UserBean>> userMaps = userBeanList.stream().collect(Collectors.groupingBy(UserBean::getId));
        for (Map.Entry<Integer, List<UserBean>> item : userMaps.entrySet()) {
            List<UserBean> value = item.getValue();
            List<UserBean> collect = value.stream().filter(x -> x.getId() == 1).collect(Collectors.toList());
            for (UserBean bean : collect) {
                bean.setName("change");
            }
        }
        for (UserBean bean : userBeanList) {
            System.out.println(bean);
        }


    }
}
