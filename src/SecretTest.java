import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * @author oweson
 * @date 2022/6/19 16:38
 */


public class SecretTest {
    /**
     * 获取某天开始时间
     *
     * @param date 日期
     * @return 开始时间
     */
    public static Date getStartTime(Date date) {
        if (null == date) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }
    public static void main(String[] args) {

        System.out.println(getStartTime(new Date()));
        System.out.println(UUID.randomUUID().toString().replace("-", ""));
        System.out.println(UUID.randomUUID().toString().replace("-", ""));
        System.out.println(UUID.randomUUID().toString().replace("-", ""));
        System.out.println(UUID.randomUUID().toString().replace("-", ""));
        System.out.println(UUID.randomUUID().toString().replace("-", ""));
        System.out.println(UUID.randomUUID().toString().replace("-", ""));

        // 服务器，mysql,redis
    }
}
