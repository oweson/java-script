package list;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author oweson
 * @date 2022/6/22 20:19
 */


public class StreamTest {
    public static void main(String[] args) {
        String collect = Stream.of("a", "b").map(String::toLowerCase).collect(Collectors.joining(","));
        System.out.println(collect);
        String hi = (String) Optional.ofNullable(null).orElse("hi");
        String hi2 =  Optional.ofNullable(null).orElse("hi").toString();
        System.out.println(hi);
        System.out.println(hi2);

        // 不可变得集合
        Collection<Object> objects = Collections.unmodifiableCollection(new ArrayList<>());


    }
}
