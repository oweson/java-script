import java.util.HashMap;
import java.util.Map;

/**
 * @author oweson
 * @date 2022/6/14 22:27
 */


public class MapTest {
    public static void main(String[] args) {
        Map<String,Object> map = new HashMap<>();
        String mapOrDefault = (String) map.getOrDefault("21", "默认值");
        Long aLong = (Long) map.getOrDefault("22", 21L);
        System.out.println(mapOrDefault);
        System.out.println(aLong);
        Object test = map.put("23", "test1");
         test = map.put("23", "test2");
        System.out.println(test);

    }
}
