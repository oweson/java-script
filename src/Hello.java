import java.util.HashMap;
import java.util.Objects;

/**
 * @author oweson
 * @date 2022/4/22 0:03
 */


public class Hello {
    public static void main(String[] args) {
        System.out.println(100);
        System.out.println(200);
        System.out.println("1/0");
        System.out.println(Objects.equals(new Integer(1), 1));

        HashMap<Long, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put(1L,21);
        Object o = objectObjectHashMap.get("1");
        System.out.println(o);
    }
}
