package j625;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author oweson
 * @date 2022/6/28 14:24
 */


public class MapTest {
    public static void main(String[] args) {
        List<UserBean> userBeanList = new ArrayList<>();
        UserBean userBean = new UserBean();
        userBean.setId(1);
        userBean.setName("one");
        UserBean userBeanSecond = new UserBean();
        userBeanSecond.setId(2);
        userBeanSecond.setName("second");
        UserBean userBeanThree = new UserBean();
        userBeanThree.setId(3);
        userBeanThree.setName("three");
        userBeanList.add(userBean);
        userBeanList.add(userBeanSecond);
        userBeanList.add(userBeanThree);
        LinkedHashMap<Integer, UserBean> integerUserBeanLinkedHashMap = userBeanList.stream()
                .collect(Collectors.toMap(UserBean::getId, Function.identity(), (x, y) -> x,
                LinkedHashMap::new));
        Hashtable<Integer, UserBean> collect3 = userBeanList.stream()
                .collect(Collectors.toMap
                        (UserBean::getId, Function.identity(), (x, y) -> x, Hashtable::new));


        // 计算每个元素的个数

        Map<Integer, Long> collect = userBeanList.stream().collect(Collectors.groupingBy(UserBean::getId, Collectors.counting()));

        List<String> list = new ArrayList<>();

        list.add("b");
        list.add("c");
        list.add("a");
        List<String> collect1 = list.stream().map(String::toUpperCase).sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        List<String> collect2 = list.stream().sorted().collect(Collectors.toList());

        System.out.println();

        boolean anyMatch = list.stream().anyMatch(x -> Objects.equals(x, "b"));

        long count = list.stream().filter(x -> Objects.equals(x, "c")).count();



    }
}
