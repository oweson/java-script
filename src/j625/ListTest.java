package j625;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author oweson
 * @date 2022/6/28 14:10
 */


public class ListTest {
    public static void main(String[] args) {

        ArrayList<String> strings = new ArrayList<>();
        strings.add("27");
        strings.add("28");
        List<Integer> collect = strings.stream().map(x -> Integer.valueOf(x)).collect(Collectors.toList());
        List<Integer> integerList = collect.stream().filter(x -> Objects.equals(x, 27)).collect(Collectors.toList());
        List<String> collect1 = strings.stream().map(x -> x.toLowerCase()).collect(Collectors.toList());
        int sum = collect.stream().mapToInt(x -> Integer.valueOf(x)).sum();
        // 反自然排序
        List<String> collect2 = strings.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());

        String orElse = strings.stream().filter(x -> Objects.equals(x, 21)).findFirst().orElse("");

        Integer integer = strings.stream().map(x -> Integer.valueOf(x)).reduce((x, y) -> x + y).orElse(0);

        List<Integer> collect3 = strings.stream().peek(x -> System.out.println(x)).map(x -> Integer.valueOf(x)).collect(Collectors.toList());


        String s = strings.stream().max(Comparator.comparing(x -> Integer.valueOf(x))).get();


    }
}
